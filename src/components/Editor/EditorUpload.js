// Libs / Helpers
import React, { Fragment } from 'react';
import styled from 'styled-components';


const EditorUpload = (props) => {
  const Input = styled.input`
    display: block;
    width: 0;
    height: 0;
    visibility: hidden;
  `;

  const Label = styled.label`
    display: block;
    width: 100%;
    padding: 10px 20px;
    border-radius: 4px;
    font-weight: 600;
    letter-spacing: 0.02em;
    background-color: white;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.25);
    border: 60px solid #333;

    :hover {
      cursor: pointer;
    }
  `;

  return (
    <Fragment>
      <Input
        className="Editor-uploader"
        type="file"
        id="file"
        multiple={ false }
        onChange={ props.onUpload }
      />
      <Label htmlFor="file">Choose a file to upload</Label>
    </Fragment>
  )
};

export default EditorUpload;
