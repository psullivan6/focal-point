import React, { Component } from 'react';
import styled from 'styled-components';

import EditorImage from './EditorImage.js';
import EditorUpload from './EditorUpload.js';

const Wrapper = styled.section`
  box-sizing: border-box;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  min-width: 300px;
  width: 30vw;
  max-width: 1000px;
  margin: 30px auto;
  transition: padding 0.6s ease;
`;

export default class Editor extends Component {
  constructor(props) {
    super(props);

    this.attachBindings();
  }

  attachBindings() {
    this.handleUpload = this.handleUpload.bind(this);
  }

  handleUpload(event) {
    const { onUpload } = this.props;
    const file = event.target.files[0];

    const image = new Image();
    image.src = window.URL.createObjectURL(file);

    // Ensure the image is fully loaded before reading its dimensions, then fire the props callback
    image.onload = () => {
      file.width = image.naturalWidth;
      file.height = image.naturalHeight;
      file.src = image.src;

      onUpload(file);
    }
  }

  render() {
    const { file, onUpdate } = this.props;

    return (
      <Wrapper active={ file != null }>
        {
          (file != null) ?
            <EditorImage
              name={ file.name }
              onUpdate={ onUpdate }
              { ...file }
            /> :
            <EditorUpload onUpload={ this.handleUpload } />
        }
      </Wrapper>
    );
  }
}