// Libs / Helpers
import React, { Component, Fragment } from 'react';
import _isEqual from 'lodash/isEqual';
import styled from 'styled-components';

// Variables
const Image = styled.img`
  display: block;
  width: 100%;
`;

const Guideline = styled.div`
  pointer-events: none;
  position: absolute;
  top: 0;
  left: 0;
  background-color: white;
  transition: transform 0.1s ease;
`;

const XAxis = styled(Guideline).attrs(({ position }) => ({
  style: {
    transform: `translateX(${position}px)`
  }
}))`
  width: 1px;
  height: 100%;
`;

const YAxis = styled(Guideline).attrs(({ position }) => ({
  style: {
    transform: `translateY(${position}px)`
  }
}))`
  width: 100%;
  height: 1px;
`;

const Point = styled.div.attrs(({ x, y }) => ({
  style: {
    transform: `translate(${x - 5}px, ${y - 5}px)`
  }
}))`
  pointer-events: none;
  visibility: ${props => props.active ? 'visible' : 'hidden'}
  position: absolute;
  top: 0;
  left: 0;
  display: block;
  width: 11px;
  height: 11px;
  border-radius: 50%;
  background-color: yellow;
  transition: transform 0.3s ease;
`


class EditorImage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active  : false,
      position: {
        x: 0,
        y: 0,
        xPercent: 0,
        yPercent: 0,
      },
      activePosition: {
        x: 0,
        y: 0,
        xPercent: 0,
        yPercent: 0,
      }
    };

    this.attachBindings();
  }

  componentDidUpdate(prevProps, prevState) {
    const { activePosition: previous } = prevState;
    const { activePosition: current } = this.state;

    if (!_isEqual(previous, current)) {
      this.props.onUpdate(current);
    }
  }

  attachBindings() {
    this.handleMove = this.handleMove.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  positionValues(event) {
    const position = event.currentTarget.getBoundingClientRect();
    const x = (event.pageX - window.scrollX - position.left);
    const y = (event.pageY - window.scrollY - position.top);

    return {
      x,
      y,
      xPercent: x / position.width,
      yPercent: y / position.height,
    };
  }

  handleMove(event) {
    this.setState({
      position: this.positionValues(event)
    });
  }

  handleClick(event) {
    const { active } = this.state;

    const state = (active) ? {
      activePosition: this.positionValues(event)
    } : {
      active: true,
      activePosition: this.positionValues(event)
    };

    this.setState(state);
  }

  render() {
    const { name, src } = this.props;
    const {
      active,
      position: { x, y },
      activePosition: { x: activeX, y: activeY }
    } = this.state;

    return (
      <Fragment>
        <Image
          src={ src }
          alt={ name }
          onMouseMove={ this.handleMove }
          onMouseUp={ this.handleClick }
        />
        <XAxis position={ x } />
        <YAxis position={ y } />
        <Point x={ activeX } y={ activeY } active={ active } />
      </Fragment>
    );
  }
}

export default EditorImage;
