import React, { useEffect, useRef, useState } from 'react';

// Components
import Box from '../../components/Box';

// Styles
import { Image } from './styles';

const Viewer = (props) => {
  const root = useRef(null);
  const [containerAspectRatio, setContainerAspectRatio] = useState(0);
  const imageSize = {
    width: props.file.width,
    height: props.file.height,
    aspectRatio: props.file.height / props.file.width,
  };

  const getImageStyles = () => {
    if (imageSize.aspectRatio >= containerAspectRatio) {
      // Get the extra height
      const maxTranslateYPercent = 1 - (containerAspectRatio / imageSize.aspectRatio);
      const translateY = `${(-1 * props.position.yPercent * (maxTranslateYPercent * 100))}%`

      return {
        width: '100%',
        transform: `translateY(${translateY})`
      }
    } else {
      const maxTranslateXPercent = 1 - (imageSize.aspectRatio / containerAspectRatio);
      const translateX = `${(-1 * props.position.xPercent * (maxTranslateXPercent * 100))}%`

      return {
        height: '100%',
        transform: `translateX(${translateX})`
      };
    }
  };

  useEffect(() => {
    const handleResize = () => {
      setContainerAspectRatio(root.current.clientHeight / root.current.clientWidth)
    };

    window.addEventListener('resize', handleResize);
    handleResize();

    setTimeout(() => {
      handleResize();
    }, 500);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <Box
      ref={root}
      position="relative"
      overflow="hidden"
      width="100%"
      height="100%"
    >
      <Image
        src={props.file.src}
        style={getImageStyles()}
      />
    </Box>
  )
};

export default Viewer;
