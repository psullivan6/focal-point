import styled from 'styled-components';

export const Image = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  transition: transform 0.3s ease;
`;

export default {};
