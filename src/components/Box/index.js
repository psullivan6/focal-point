import styled from 'styled-components';
import {
  color,
  flexbox,
  typography,
  layout,
  position,
  space
} from 'styled-system';

const Box = styled.div(
  {
    boxSizing: 'border-box',
    minWidth: 0,
  },
  color,
  flexbox,
  typography,
  layout,
  position,
  space
);

export default Box;
