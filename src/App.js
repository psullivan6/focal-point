import React, { Component, Fragment } from 'react';
import { Resizable } from "re-resizable";

import './App.css';

import Editor from './components/Editor/Editor.js';
import Viewer from './components/Viewer';
import Box from './components/Box';


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      file: null,
      position: {
        x: 0,
        y: 0,
        xPercent: 0,
        yPercent: 0,
      }
    };

    this.attachBindings();
  }

  attachBindings() {
    this.handleUpload = this.handleUpload.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpload(file) {
    this.setState({ file });
  }

  handleUpdate(position) {
    this.setState({ position });
  }

  render() {
    const { file, position } = this.state;

    const handleResize = () => {
      window.dispatchEvent(new Event('resize'));
    }

    return (
      <div className="App">
        { file != null && (
          <p style={{ display: 'block', width: '100%' }}>👇 Move your mouse and click on the desired focal point</p>
        )}
        <Editor
          onUpload={ this.handleUpload }
          onUpdate={ this.handleUpdate }
          file={ file }
        />
        <hr />
        {
          file && (
            <Fragment>
              <p>👇This container can be resized for easier testing, just drag the corner or the sides of the image</p>
              <Box
                as={Resizable}
                backgroundColor="#ccc"
                style={{ border: '1px solid black' }}
                margin="1rem"
                defaultSize={{
                  width:320,
                  height:200,
                }}
                onResizeStop={handleResize}
              >
                <Viewer
                  file={ file }
                  position={position}
                />
              </Box>
              <hr />
              <p>👇Some more example container sizes</p>
              <Box
                display="flex"
                flexWrap="wrap"
                justifyContent="space-between"
                maxWidth="1200px"
                margin="auto"
              >
                <Box
                  width="25%"
                  height="30rem"
                  style={{ border: '1px solid black' }}
                >
                  <Viewer
                    file={ file }
                    position={position}
                  />
                </Box>

                <Box
                  width="75%"
                  height="15rem"
                  style={{ border: '1px solid black' }}
                >
                  <Viewer
                    file={ file }
                    position={position}
                  />
                </Box>

                <Box
                  width="10%"
                  height="25rem"
                  style={{ border: '1px solid black' }}
                >
                  <Viewer
                    file={ file }
                    position={position}
                  />
                </Box>

                <Box
                  width="70%"
                  height="30rem"
                  style={{ border: '1px solid black' }}
                >
                  <Viewer
                    file={ file }
                    position={position}
                  />
                </Box>
              </Box>
            </Fragment>
          )
        }
      </div>
    );
  }
}

export default App;
